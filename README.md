## Installation

```bash
$ npm install
```

## Init database by docker (optional)

you can create the postgres database via "docker compose up"

## Migrate the database

```bash
$ npx prisma migrate dev
```

please change the database_url in .env to your database

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

```

## default port :3000

## API document (swagger): /api

example: https://simple-cart-backend.onrender.com/api
