import {
  HttpCode,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ProductsService {
  constructor(private prismaService: PrismaService) {}
  async create(createProductDto: CreateProductDto) {
    const newProduct = await this.prismaService.product.create({
      data: createProductDto,
    });

    //Throw error if new product can be created
    if (newProduct == null) {
      throw new HttpException(
        'Create new Product failed!',
        HttpStatus.BAD_REQUEST,
      );
    }

    return newProduct;
  }

  async findAll() {
    const allProducts = await this.prismaService.product.findMany();
    return allProducts;
  }

  async findOne(id: number) {
    const product = await this.prismaService.product.findUnique({
      where: { id: id },
    });

    //Throw exception if no product found
    if (product === null) {
      throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
    }

    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    //Check if the product existed in the database
    const product = await this.findOne(id);

    const updatedProduct = await this.prismaService.product.update({
      where: {
        id: id,
      },
      data: updateProductDto,
    });

    return updateProductDto;
  }

  async remove(id: number) {
    const product = await this.findOne(id);
    try {
      const result = await this.prismaService.product.delete({
        where: {
          id: id,
        },
      });
    } catch (err) {
      throw new HttpException(
        "Can't delete this product",
        HttpStatus.BAD_REQUEST,
      );
    }

    return {
      message: `Delete product with id: ${id} successfully`,
    };
  }
}
