import { ApiProperty } from '@nestjs/swagger';
import { CreateProductDto } from '../dto/create-product.dto';
import { IsNumber } from 'class-validator';

export class Product extends CreateProductDto {
  @ApiProperty()
  @IsNumber()
  id: number;
}
